# xrcb-scheduler

Motivation:

- demo/usecase of just a set of [liquidsoap](http://savonet.sourceforge.net/) scripts trying to replace airtime/libretime
- the intended place to show transparently how is managing the streamings the xrcb.cat site, concretely, this is the /home/liquidsoap location of xrcb.cat, and the place where reside original scripts of the site (with the exception of passwords, secrets and dotfiles).

extra info about installation: https://github.com/guifi-exo/wiki/blob/master/howto/radio-audio-streaming.md#liquidsoap

## explaining some scripts

- *generate_playlist.sh* : scan for mp3 files to include in a playlist
- *prepare_template_scheduler.py* : python script that reads schedule through xrcb.cat API (wordpress site) and prepare the files to be played today
- *xrcb-scheduler.cron* : put this file as */etc/cron.d/xrcb-scheduler*

## tracks overlapped in our schedule

I run this tests with liquidsoap 1.3.3

Assuming time is in the overlap moment (for example when time is 4h45m) and being the next situation:

```
({ 4h30m0s-5h30m0s }, single('/path/to/track1.mp3')),
({ 4h0m0s-5h0m0s }, single('/path/to/track2.mp3')),
```

in a logical situation we should play both, or complain about two simultaneous ways. But it resolves always starting to play track1 because is the first line

Hence, if we swap the lines this way:

```
({ 4h30m0s-5h30m0s }, single('/path/to/track1.mp3')),
({ 4h0m0s-5h0m0s }, single('/path/to/track2.mp3')),
```

always plays track2

always means that is not random

## icecast API: Currently playing

if you want to use title and artist (artist is hidden if is null) for your player

    cp mounts.xsl /usr/local/share/icecast/web
    chown icecast2: /usr/local/share/icecast/web/mounts.xsl

then you can consume the resource here http://example.com:8000/mounts.xsl

documentation: http://icecast.org/docs/icecast-2.4.1/server-stats.html

custom examples:

- (I used this) https://github.com/MechanisM/jquery-icecast/blob/master/web/json.xsl
    - https://github.com/MechanisM/jquery-icecast
- https://forum.sourcefabric.org/discussion/14631/simple-way-to-show-currently-playing-title-from-icecast/p1
- http://linge-ma.ws/update-listeners-track-on-a-website-using-icecast-jsonp-and-jquery/

## compile custom icecast and liquidsoap

https://gitlab.com/guifi-exo/wiki/blob/master/howto/radio-audio-streaming.md
