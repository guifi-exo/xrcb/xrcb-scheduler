# where to scan mp3 files
media_url="/var/www/html/wordpress/wp-content/uploads/"
output_file="/home/liquidsoap/playlist.txt"

# thanks https://www.cyberciti.biz/tips/howto-linux-unix-find-move-all-mp3-file.html
#   that was confusing
#find "$media_url" -iname "*.mp3" -print > "$output_file"
cat > "${output_file}" <<EOF
/var/www/html/wordpress/wp-content/uploads/2018/10/01-fm-arp-kick-2.mp3
/var/www/html/wordpress/wp-content/uploads/2018/10/02-fm-bossa-2.mp3
/var/www/html/wordpress/wp-content/uploads/2018/10/03-fm-bossa.mp3
/var/www/html/wordpress/wp-content/uploads/2018/10/04-fm-float.mp3
/var/www/html/wordpress/wp-content/uploads/2018/10/05-fm-lcd.mp3
/var/www/html/wordpress/wp-content/uploads/2018/10/09-data-center.mp3
/var/www/html/wordpress/wp-content/uploads/2018/10/10-homeopathic-music.mp3
EOF

chown liquidsoap: "$output_file"
