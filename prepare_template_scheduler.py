#debug
#import code; code.interact(local=dict(globals(), **locals()))

# particular processing given the current API of xrcb.cat
def xrcb_processing(p):
  # parse start time [start 18:00:00 => lstart 18h0m0s]
  pt = [int(s) for s in p['start'].split(':')] # podcast time
  p['lstart'] = '{}h{}m{}s'.format(pt[0],pt[1],pt[2])
  # find stop time (lstop) -> src/thanks https://stackoverflow.com/a/2780968
  p_totalSecs = (pt[0] * 60 + pt[1]) * 60 + pt[2]
  if p['live']:
    p['audio']['lurl'] = ''
    totalSecs = p_totalSecs + p['live_duration']
  else:
    # parse file path
    # [url: replace from https url to file in server]
    p['audio']['lurl'] = p['audio']['url'].replace('https://xrcb.cat','/var/www/html/wordpress')
    totalSecs = p_totalSecs + p['audio']['meta']['length']
  totalSecs, s = divmod(totalSecs, 60)
  h, m = divmod(totalSecs, 60)
  h = h % 24
  p['lstop'] = '{}h{}m{}s'.format(h,m,s)

  return p

# jinja templating
# requirement: apt install python3-jinja2
from jinja2 import Template

# API request
import requests

# for full path joins
import os

# following API request gets all scheduled
# radio programs (in mp3 file) for the same day
url = 'https://xrcb.cat/ca/programacio-json'

base_path = '/home/liquidsoap/script/'

# thanks https://stackoverflow.com/questions/35120250/python-3-get-and-parse-json-api
r = requests.get(url)
podcasts = r.json()

# parse variables for liquidsoap
for p in podcasts['podcasts']:
  p = xrcb_processing(p)

# get template and render now (according to schedule)
tfile = open(os.path.join(base_path,'schedule.liq.j2'),'r').read()
template = Template(tfile)
thatrender = template.render(podcasts)

outputFile = open(os.path.join(base_path,'schedule.liq'), 'w')
outputFile.write(thatrender)
